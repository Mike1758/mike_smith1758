package test;
import test_selenium_java.pages.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class SoftwareTest  extends TestBase   {
	
	
	@Test()
	public void SelectorWhichSortOnListNumberTest() throws InterruptedException
	{
		SoftwareTestingPage test = new SoftwareTestingPage(webDriver);
		test.startUrl();
		Assert.assertEquals("���������� ���������", test.getHeadSceldue());
		
		test.sortingByInputNumer("3");
		Assert.assertEquals(3, test.getCountElementSort());

		test.sortingByInputNumer("5");
		Assert.assertEquals(5, test.getCountElementSort());
		
		test.sortingByInputNumer("10");
		Assert.assertEquals(10, test.getCountElementSort());
	}
	
	@Test
	public void SelectorsCategoryesTest() throws InterruptedException
	{
		SoftwareTestingPage test = new SoftwareTestingPage(webDriver);
		test.openTrainingUrl();
		test.sendTextInSearch("������� ����������");
		
		Assert.assertEquals("����������� ������� ���������� ������������� �� ��������� ISTQB FL", test.getFirstLink());
		
		test.clickAllCategory();
		Assert.assertEquals(test.getTitleName(), test.getCategory());
		
		test.clickShortOnlineCouses();
		Assert.assertEquals(test.getTitleName(), test.getCategory());

		test.clickOnlineCouses();
		Assert.assertEquals(test.getTitleName(), test.getCategory());
		
		test.clickOfflineTraining();
		Assert.assertEquals(test.getTitleName(), test.getCategory());
		
		test.clickConferense();
		Assert.assertEquals(test.getTitleName(), test.getCategory());	
	}
	
	@Test
	public void clickOnPagesTest() throws InterruptedException
	{
		SoftwareTestingPage test = new SoftwareTestingPage(webDriver);
		test.openTrainingUrl();
		test.clickButtonClearAll();
		test.clickNextPage();
		
		Assert.assertEquals("�������� 2/9", test.getCurrentPage());
		
		test.clickOnNumberPage("4");
		Assert.assertEquals("�������� 4/9", test.getCurrentPage());
		
		test.clickPreviousPage();
		Assert.assertEquals("�������� 3/9", test.getCurrentPage());
		
		test.clickFinalPage();
		Assert.assertEquals("�������� 9/9", test.getCurrentPage());
		
		test.clickFirstPage();
		Assert.assertEquals("�������� 1/9", test.getCurrentPage());
		
		test.clickOnNumberPage("5");
		Assert.assertEquals("�������� 5/9", test.getCurrentPage());
	}
}
