package test;
import test_selenium_java.pages.*;
import org.junit.Assert;
import org.testng.annotations.Test;


public class SoftwareAutorizationTest extends TestBase {
//@Test
	public void checkRegistrationTest() throws InterruptedException{
		SoftwareAutorizationPage test = new SoftwareAutorizationPage(webDriver);
		test.openJobPage();
		test.clearLoginField();
		test.Autorization("mike_smith_1796", "1597532");
		Assert.assertTrue(test.getError().contains("������"));
		
		test.openJobPage();
		Assert.assertTrue(test.getloginFieldStartText().isEmpty());
		Assert.assertTrue(test.getpasswordFieldStartText().isEmpty());
		
		test.openJobPage();
		test.clearLoginField();
		test.clearPasswordField();
		test.Autorization("mike_smith_1796", "1597532");
		Assert.assertEquals("������������, Mike_smith_1796", test.getLoginSuccess());
	
	}

//@Test
	public void checkLinkForgotAndRegisrtationTest(){
		SoftwareAutorizationPage test = new SoftwareAutorizationPage(webDriver);
		test.openJobPage();
		test.clickByForgotLogin();
		Assert.assertEquals("������ �����?", test.getRememberOrRegistry());
	
		test.openJobPage();
		test.clickByForgotPassword();
		Assert.assertEquals("������ ������?", test.getRememberOrRegistry());
	
		test.openJobPage();
		test.clickRegistration();
		Assert.assertEquals("�����������", test.getRememberOrRegistry());
	
		test.clickCheckBox();
		boolean checkBoxValue = test.getCheckBox().isSelected();
		Assert.assertTrue(checkBoxValue);
	}

//@Test
	public void checkFailedRegistrationPageTest() throws InterruptedException{
	SoftwareAutorizationPage test = new SoftwareAutorizationPage(webDriver);
	SoftwareRegistrationPage testing = new SoftwareRegistrationPage(webDriver);
	test.openJobPage();
	test.clickRegistration();
	Assert.assertEquals("�����������", test.getRememberOrRegistry());
	
	testing.sendUserName("");
	testing.sendLogin("&151");
	testing.sendEmail("asdg");
	Assert.assertEquals(testing.getColorName(),testing.redColor);
	Assert.assertEquals(testing.getColorLogin(),testing.redColor);
	
	testing.sendPassword("");
	Assert.assertEquals(testing.getColorEmail(),testing.redColor);
	
	testing.sendReturnPassword("14125");
	Assert.assertEquals(testing.getColorPassword(),testing.redColor);
	
	testing.clickByUsername();
	Assert.assertEquals(testing.getColorPasswordReturn(),testing.redColor);
}

	//@Test
	public void succsessRegistrationTest() throws InterruptedException{
	SoftwareAutorizationPage test = new SoftwareAutorizationPage(webDriver);
	SoftwareRegistrationPage testing = new SoftwareRegistrationPage(webDriver);
	test.openJobPage();
	test.clickRegistration();
	Assert.assertEquals("�����������", test.getRememberOrRegistry());
	
	testing.sendUserName("mikesqw");
	testing.sendLogin("mike_laleotyqwtqwtt");
	Assert.assertEquals(testing.getColorName(),testing.whiteColorTextHead);
	
	testing.sendEmail("qwertasdzxvgqwtqw@mail.ru");
	Assert.assertEquals(testing.getColorLogin(),testing.whiteColorTextHead);
	
	testing.sendPassword("qwerty123");
	Assert.assertEquals(testing.getColorEmail(),testing.whiteColorTextHead);
	
	testing.sendReturnPassword("qwerty123");
	Assert.assertEquals(testing.getColorPassword(),testing.whiteColorTextHead);
	
	testing.clickByUsername();
	Assert.assertEquals(testing.getColorPasswordReturn(),testing.whiteColorTextHead);
	
	testing.clickByButonRegistration();
	Assert.assertEquals(testing.getTextSuccsess(), "������� ������ �������. �� ��������� ���� ����� ����������� ����� ���������� ������ � ����������� �� ���������.");
	}

	@Test
	public void thisMailOrUsernameIsUsedTest()
	{
	SoftwareAutorizationPage test = new SoftwareAutorizationPage(webDriver);
	SoftwareRegistrationPage testing = new SoftwareRegistrationPage(webDriver);
	test.openJobPage();
	test.clickRegistration();
	Assert.assertEquals("�����������", test.getRememberOrRegistry());
	
	testing.sendUserName("mikesqw");
	testing.sendLogin("mike_laleoty");
	Assert.assertEquals(testing.getColorName(),testing.whiteColorTextHead);
	
	testing.sendEmail("lolsdshsddsgsdddasgadhdstg@mail.ru");
	Assert.assertEquals(testing.getColorLogin(),testing.whiteColorTextHead);
	
	testing.sendPassword("qwerty123");
	Assert.assertEquals(testing.getColorEmail(),testing.whiteColorTextHead);
	
	testing.sendReturnPassword("qwerty123");
	Assert.assertEquals(testing.getColorPassword(),testing.whiteColorTextHead);
	
	testing.clickByUsername();
	Assert.assertEquals(testing.getColorPasswordReturn(),testing.whiteColorTextHead);
	
	testing.clickByButonRegistration();
	Assert.assertEquals(testing.getTextErorMessage(), "������� ������ � ����� ������ ������������ ��� ����������. ����������, ���������� ������ ��������.");
	
	}
}
