package test_selenium_java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {
	
	protected WebDriver driver;

	public PageBase(WebDriver driver) {
        this.driver = driver;
    }
	
	public void openUrl(String text){
		driver.get(text);
	}
	 
	 public void clickByElement(By element){
		 waitElement(element);
		 driver.findElement(element).click();
	 }
	 
	 public void sendText(By element, String text){
		 waitElement(element);
		 driver.findElement(element).sendKeys(text);
	 }
	 
	 public void sendKey(By element, Keys key){
		 waitElement(element);
		 driver.findElement(element).sendKeys(key);
	 }
	 
	 public String getText(By element){
		 waitElement(element);
		 String text = driver.findElement(element).getText();
		 return text;
	 }
	 
	 public void waitElement(By element){
		 (new WebDriverWait(driver, 10))
	        .until(ExpectedConditions.visibilityOfElementLocated(element));
	 }
	 
	 public void waitDisappearElement(By element){
		 (new WebDriverWait(driver, 10))
	        .until(ExpectedConditions.invisibilityOfElementLocated(element));
	 }
}
