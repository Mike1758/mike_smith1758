package test_selenium_java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class SoftwareRegistrationPage extends PageBase {

	public SoftwareRegistrationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//public String redColorsTextHead="rgba(102, 102, 102, 1)";
	
	public String redColor = "rgba(255, 0, 0, 1)";
	
	public String whiteColorTextHead = "rgba(0, 0, 0, 1)";
	
	By userNameField = By.id("name");
	
	By userLoginField = By.id("username");
	
	By emailField = By.id("email");
	
	By passwordField = By.id("password");
	
	By passwordReturnField = By.id("password2");
	
	By emailHead = By.id("emailmsg");
	
	By userNameHead = By.id("usernamemsg");
	
	By nameUserHead = By.id("namemsg");
	
	By passwordHead = By.id("pwmsg");
	
	By passwordReturnHead = By.id("pw2msg");
	
	By registrationButton = By.xpath("//*[@class='button validate']");
	
	By  errorMessageHead = By.xpath("//*[@class='error message fade']");
	
	By errorWindowHead = By.xpath("//*[@class='error']");
	
	By succsessWindowHead = By.xpath("//*[@class='message message fade']");
	
	public void sendUserName(String text){
		sendText(userNameField, text);
	}
	
	public void sendLogin(String text){
		clickByElement(userLoginField);
		sendText(userLoginField, text);
	}
	
	public void sendEmail(String text){
		clickByElement(emailField);
		sendText(emailField, text);
	}

	public void sendPassword(String text){
		clickByElement(passwordField);
		sendText(passwordField, text);
	}
	
	public void sendReturnPassword(String text){
		clickByElement(passwordReturnField);
		sendText(passwordReturnField, text);
	}
	
	public void clickByUsername(){
		clickByElement(userNameField);
	}
	
	public String getColorEmail(){
		return driver.findElement(emailHead).getCssValue("color");
	}
	
	public String getColorName(){
		return driver.findElement(userNameHead).getCssValue("color");
	}
	
	public String getColorLogin(){
		return driver.findElement(nameUserHead).getCssValue("color");
	}
	
	public String getColorPassword(){
		return driver.findElement(passwordHead).getCssValue("color");
	}
	
	public String getColorPasswordReturn(){
		return driver.findElement(passwordReturnHead).getCssValue("color");
	}
	
	public void clickByButonRegistration(){
		clickByElement(registrationButton);
	}
	
	public String getTextErorMessage(){
		waitElement(errorWindowHead);
		return driver.findElement(errorMessageHead).getText();	
	}
	
	public String getTextSuccsess(){
		waitElement(succsessWindowHead);
		return driver.findElement(succsessWindowHead).getText();
	}
}
