package test_selenium_java.pages;
import java.util.List;
import org.openqa.selenium.support.ui.Select;



import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class SoftwareTestingPage extends PageBase {
	public SoftwareTestingPage(WebDriver driver) {
		super(driver);
	}
public static final String startUrl = "http://software-testing.ru/";

public static final String trainingUrl = "http://software-testing.ru/trainings";
	
	String searchRequestText  = "������� ����������";
	
	By trainingLink = By.xpath("//*[text()='��������']");
	
	By scelduePageHeader = By.xpath("//div[text()='���������� ���������']");
	
	By listOfLinks = By.xpath("//*[@class='sem_row']/..");
	
	By allCategorySelect = By.xpath("//option[text()='��� ���������']");
	
	By shortOnlineCourseSelect = By.xpath("//option[text()='�������� ������-��������']");
	
	By onlineCourseSelect = By.xpath("//option[text()='������-��������']");
	
	By fullTimeCourseSelect = By.xpath("//option[text()='����� ��������']");
	
	By conferenseSelect = By.xpath("//option[text()='�����������']");
	
	By kategoryHeader = By.xpath("//*[@class='sem_cat_title']");
	
	By kategorySelector = By.xpath("//*[@name='catid']");
	
	By searchField = By.name("search");
	
	By nextPageLink = By.xpath("//*[text()='���������'][1]");
	
	By firstSearchLink = By.xpath("//*[text()='����������� ������� ���������� ������������� �� ��������� ISTQB FL']");
	
	By clearAllButtonLink = By.xpath("//*[text()='�������� ������']");

	By finalPageLink = By.xpath("(//a[text()='���������'])[1]");
	
	By previousPageLink = By.xpath("(//a[text()='����������'])[1]");
	
	By firstPageLink = By.xpath("(//a[text()='������'])[1]");
	
	By currentPageHeader = By.xpath("(//*[@class='sem_nav'])[5]");//!

	public int getCountElementSort(){
		return driver.findElements(listOfLinks).size();
	}
	
	public void sortingByInputNumer(String count){
		String text = "(//option[text()=" + count + "])";
		By sortingNumber = By.xpath(text);
		clickByElement(sortingNumber);
	}
	
	public void startUrl(){
		openUrl(startUrl);
	}
	
	public void openTrainingUrl(){
		openUrl(trainingUrl);
	}
	
	public String getHeadSceldue(){
		clickByElement(trainingLink);
		return driver.findElement(scelduePageHeader).getText();
	}
	
	public void clickOnNumberPage(String count){
		String text = "(//a[text()=" + count + "])[1]" ;
		By sortingNumber = By.xpath(text);
		clickByElement(sortingNumber);
	}
	
	public void clickAllCategory(){
		clickByElement(allCategorySelect);
	}
	
	public String getAllCategory(){
		String text = driver.findElement(allCategorySelect).getText();
		return text;
	}
	
	public void clickShortOnlineCouses(){
		clickByElement(shortOnlineCourseSelect);
	}
	
	public String getShortOnlineCourses(){
		String text = driver.findElement(shortOnlineCourseSelect).getText();
		return text;
	}
	
	public void clickOnlineCouses(){
		clickByElement(onlineCourseSelect);
	}
	
	public String getOnlineCourses(){
		clickByElement(onlineCourseSelect);
		String text = driver.findElement(onlineCourseSelect).getText();
		return text;
	}
	
	public void clickOfflineTraining(){
		clickByElement(fullTimeCourseSelect);
	}
	
	public String getOfflineTraining(){
		String text = driver.findElement(fullTimeCourseSelect).getText();
		return text;
	}
	
	public void clickConferense(){
		clickByElement(conferenseSelect);
	}
	
	public String getConferense(){
		return driver.findElement(conferenseSelect).getText();
	}
	
	public String getTitleName(){
		return driver.findElement(kategoryHeader).getText();
	}
	
	public String getCategory(){
		Select select = new Select(driver.findElement(kategorySelector));
		return select.getFirstSelectedOption().getText();
		
	}
	
	public void sendTextInSearch(String text){
		sendText(searchField, text);
		sendKey(searchField, Keys.ENTER);
	}
	
	public String getFirstLink(){
		return driver.findElement(firstSearchLink).getText();
	}
	
	public void clickButtonClearAll(){
		clickByElement(clearAllButtonLink);
		sortingByInputNumer("3");
	}
	
	public void clickNextPage(){
		clickByElement(nextPageLink);
	}
	
	public void clickFinalPage(){
		clickByElement(finalPageLink);
	}
	
	public void clickPreviousPage(){
		clickByElement(previousPageLink);
	}
	
	public void clickFirstPage(){
		clickByElement(firstPageLink);
	}
	
	public String getCurrentPage(){
		String text = driver.findElement(currentPageHeader).getText();
		return text;
	}
}

