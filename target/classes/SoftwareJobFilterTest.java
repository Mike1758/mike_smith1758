package test;
import org.testng.Assert;
import org.testng.annotations.Test;

import test_selenium_java.pages.SoftwareJobPage;

public class SoftwareJobFilterTest extends TestBase {
	
	@Test
	public void checkSearchTest(){
		SoftwareJobPage test = new SoftwareJobPage(webDriver);
		
		test.openJobUrl();
		test.sendPostText("�����������");
		Assert.assertEquals("Senior QA Engineer", test.getFirstLinkPostSearch());
	
		test.openJobUrl();
		test.sendRegionText("������");
		Assert.assertEquals("�������-����������� (QA Engineer) � ��������� ������� ������", test.getFirstLinkRegion());
	}

    @Test
	public void checkTypeOfTest(){
		SoftwareJobPage test = new SoftwareJobPage(webDriver);
		
		test.openJobUrl();
		test.clickOnAutomatizationTesting();
		String TypeOfTesting = test.getTypeOfTesting();
		test.clickByButtonSearch();
		Assert.assertEquals(test.getFirstLinkTypeOfTesting(), TypeOfTesting);
	
		test.openJobUrl();
		test.clickOnTesting();
		TypeOfTesting = test.getTypeOfTesting();
		test.clickByButtonSearch();
		Assert.assertEquals(test.getFirstLinkTypeOfTesting(), TypeOfTesting);
	
		test.openJobUrl();
		test.clickOnManageTesting();
		TypeOfTesting = test.getTypeOfTesting();
		test.clickByButtonSearch();
		Assert.assertEquals(test.getFirstLinkTypeOfTesting(), TypeOfTesting);
	}

    @Test
	public void checkLevelOfIncomeTest(){
		SoftwareJobPage test = new SoftwareJobPage(webDriver);
		
		test.openJobUrl();
		test.clickOnbefore600Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickwith600Before800Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickwith800Before1100Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickwith1100Before1500Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickwith1500Before2000Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickwith2000Before2500Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	
		test.openJobUrl();
		test.clickhigth2500Selector();
		Assert.assertEquals(test.getLevelIncome(), test.getFirstLinkLevelIncome());
	}
}
