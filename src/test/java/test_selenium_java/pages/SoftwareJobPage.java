package test_selenium_java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SoftwareJobPage extends PageBase {

	public SoftwareJobPage(WebDriver driver) {
		super(driver);
	}
	
	String startUrl = "http://software-testing.ru/job/";
	
	By postSearchField = By.xpath("//*[@class = 'input_field_keywords']");
	
	By categoryTypeofTestSelector = By.xpath("//*[@id= 'job_cat']");
	
	By levelIncomeSelector = By.name("db_lknjobs_jobsalary");
	
	By regionSearchField = By.xpath("//*[@class = 'input_field_location']");
	
	By searchButton = By.id("qsbButton");
	
	By postFirstLink = By.xpath("//*[@href = '/job/index.php?option=com_jobs&task=detail_job&id=2175:senior-qa-engineer&Itemid=5']");
	
	By regionFirstLink = By.xpath("//*[@href = '/job/index.php?option=com_jobs&task=detail_job&id=2163:--qa-engineer----&Itemid=5']");
	
	By automationTestingSelect = By.xpath("//option[text()='������������� ������������']");
	
	By testingSelect = By.xpath("//option[text()='������������']");
	
	By manageTestingSelect = By.xpath("//option[text()='���������� �������������']");
	
	By typeOfTestingLink = By.xpath("(//*[@class='company'])[3]");
	
	By levelIncomeFirstLink = By.xpath("(//*[@class='company salary'])[1]");
	
	By before600Select = By.xpath("//*[text()='�� 600$']");
	
	By with600Before800Select = By.xpath("//*[text()='600-800$']");
	
	By with800Before1100Select = By.xpath("//*[text()='800-1100$']");
	
	By with1100Before1500Select = By.xpath("//*[text()='1100-1500$']");
	
	By with1500Before2000Select = By.xpath("//*[text()='1500-2000$']");
	
	By with2000Before2500Select = By.xpath("//*[text()='2000-2500$']");
	
	By higth2500Select = By.xpath("//*[text()='����� 2500$']");
	
	public void openJobUrl(){
		openUrl(startUrl);
	}
	
	public void sendPostText(String element){
		sendText(postSearchField, element);
		clickByButtonSearch();
	}
	
	public void clickByButtonSearch(){
		clickByElement(searchButton);
	}
	
	public String getFirstLinkPostSearch(){
		return driver.findElement(postFirstLink).getText();
	}
	
	public void sendRegionText(String element){
		sendText(regionSearchField, element);	
		clickByButtonSearch();
	}
	
	public String getFirstLinkRegion(){
		return driver.findElement(regionFirstLink).getText();
	}
	
	public void  clickOnAutomatizationTesting(){
		clickByElement(automationTestingSelect);
	}
	
	public void  clickOnTesting(){
		clickByElement(testingSelect);
	}
	
	public void  clickOnManageTesting(){
		clickByElement(manageTestingSelect);
	}
	
	public String getFirstLinkTypeOfTesting(){
		return driver.findElement(typeOfTestingLink).getText();
	}
	
	public String getTypeOfTesting(){
		Select select = new Select(driver.findElement(categoryTypeofTestSelector));
		return select.getFirstSelectedOption().getText();
	}
	
	public void clickOnbefore600Selector(){
		clickByElement(before600Select);
	}
	
	public void clickwith600Before800Selector(){
		clickByElement(with600Before800Select);
		
	}
	
	public void clickwith800Before1100Selector(){
		clickByElement(with800Before1100Select);
	}
	
	public void clickwith1100Before1500Selector(){
		clickByElement(with1100Before1500Select);
	}
	
	public void clickwith1500Before2000Selector(){
		clickByElement(with1500Before2000Select);
	}
	
	public void clickwith2000Before2500Selector(){
		clickByElement(with2000Before2500Select);
	}
	
	public void clickhigth2500Selector(){
		clickByElement(higth2500Select);
	}
	
	public String getLevelIncome(){
		Select select = new Select(driver.findElement(levelIncomeSelector));
		String text = select.getFirstSelectedOption().getText();
		clickByButtonSearch();
		return text;
	}
	
	public String getFirstLinkLevelIncome(){
		return driver.findElement(levelIncomeFirstLink).getText();
	}
	
	public void openJobPage(){
		openUrl(startUrl);
	}
}

