package test_selenium_java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class SoftwareAutorizationPage extends PageBase {

	public SoftwareAutorizationPage(WebDriver driver) {
		super(driver);
	}

	String startUrl = "http://software-testing.ru/job/";
	
	By loginField = By.id("modlgn_username");
	
	By passwordField = By.id("modlgn_passwd");
	
	By loginButton = By.xpath("//*[@type ='image']");
	
	By rememberCheckBox = By.id("modlgn_remember");
	
	By forgotPasswordLink =  By.xpath("//*[@href ='/job/index.php?option=com_user&view=reset']");
	
	By forgotLoginLink = By.xpath("//*[@href ='/job/index.php?option=com_user&view=remind']");
	
	By registrationLink = By.xpath("//*[@href ='/job/index.php?option=com_user&task=register']");
	
	By messageInWindowField = By.id("system-message");
	
	By forgotOrRegistrationHead = By.xpath("//*[@class = 'componentheading']");
	
	By successLoginHead = By.xpath("//*[@id='form-login']");
	
	
	
	public void openJobPage(){
		openUrl(startUrl);
	}
	
	public void clickByForgotPassword(){
		clickByElement(forgotPasswordLink);
	}
	
	public void clickByForgotLogin(){
		clickByElement(forgotLoginLink);
	}
	
	public void clickRegistration(){
		clickByElement(registrationLink);
	}
	
	public void clearLoginField(){
		driver.findElement(loginField).clear();
	}
	
	public void clearPasswordField(){
		driver.findElement(passwordField).clear();
	}
	
	public void sendLoginText(String text){
		sendText(loginField, text);
	}
	
	public void sendPasswordText(String text){
		sendText(passwordField, text);
	}
	
	public String getRememberOrRegistry(){
		return driver.findElement(forgotOrRegistrationHead).getText();
	}
	
	public void clickCheckBox(){
		clickByElement(rememberCheckBox);
	}
	
	public void clickButtonLogin(){
		clickByElement(loginButton);
	}
	
	public WebElement getCheckBox(){
		WebElement element = driver.findElement(rememberCheckBox);
		return element;
	}
	
	public String getError(){
		return driver.findElement(messageInWindowField).getText();
	}
	
	public String getLoginSuccess(){
		return driver.findElement(successLoginHead).getText();
	}
	
	public void Autorization(String textLogin, String textPassword){
		sendLoginText(textLogin);
		sendPasswordText(textPassword);
		clickButtonLogin();
	}
	
	public String getloginFieldStartText(){
		return driver.findElement(loginField).getText();
	}
	
	public String getpasswordFieldStartText(){
		return driver.findElement(passwordField).getText();
	}
}
