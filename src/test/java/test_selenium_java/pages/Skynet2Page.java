package test_selenium_java.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Skynet2Page extends PageBase{

	public Skynet2Page(WebDriver driver) {
		super(driver);
	}
	

	public static final String hostedTable3Url = "http://alpha.skynet.managementevents.com/events/2316/table-settings/1";
			
	public static String loginName = "xsolve";
			
	public static String passwordName = "xs0lv3";
			
			By loginField = By.id("username");
			
			public By lol = By.id("lol");
			
			By passwordField = By.id("password");
			
			By autorizationButton = By.xpath("//*[@class='btn btn-primary']");
			
			By numberOfDelegatOnTableHead = By.xpath("//*[@title='amount of delegates seated in any table (hosted + extra)']/..");
			
			By countofFirstTableHead = By.xpath("//*[@class='col-sm-6 text-right']");
			
			By deligateOnTheTableLinks = By.xpath("//*[@class='isDelegate']");
			
			By deligateWithoutTableFirstPartLinks = By.xpath("//*[@class='isDelegate isBlocked isOrphan']");
			
			By deligateWithoutTableSecondPartLink = By.xpath("//*[@class='isDelegate isOrphan']");
			
			By numberOfDelegateWithoutTableHead = By.xpath("//*[@title='amount of delegates not yet seated in any table']/..");
			
			By countOfTablesLinks = By.xpath("(//th[contains(text(), '#')])");
			
			By countExtraTableHead = By.xpath("//*[@title='the number of tables that are not hosted by a partner representative']/..");
			
			By hostedTableAllHead = By.xpath("//*[@title='the number of tables that have a partner host assigned']/..");
	
			By countOfPartnerLink = By.xpath("//*[@class='info isPartner']");
			
			By countOfPartnerHead = By.xpath("//*[@title='amount of reps seated in tables']/..");
			
			By countOfFirstTableLink = By.xpath("(//*[@class='isDelegate']/..)[1]");
			
			By countOfTableHead = By.xpath("(//*[@class='col-sm-6 text-right'])[3]");
			
			By moreInfoAboutPeopleLink = By.xpath("(//tr/td/b)[1]");
			
			By infoWindowHead = By.xpath("//*[@class='tooltip in tooltip-top top']");
			
			By numberOfPeopleInTableHead = By.xpath("(//div/div/me-table-seating/table/tbody)[1]/tr");
			
			By countOfBannedLink = By.xpath("//div/div/me-table-seating/table/tbody/tr/td/span[@class='pull-right']");
			
			
			public void Autification(){
				openUrl(hostedTable3Url);
				sendText(loginField,loginName);
				sendText(passwordField,passwordName);
				clickByElement(autorizationButton);
			}
			
			public String getRegexText(By locator){
				String text = driver.findElement(locator).getText();
				text = text.replaceAll("[a-z]|[A-Z]|\\:|\\ ","");
				return text;
			}
			
			public String getDeligateOnTableText(){
				return getRegexText(numberOfDelegatOnTableHead);
			}
			
			public String getCountDeligateOnTableLinks(){
				if(infoAboutBannedPeople())
					return getCountDeligateOnTableLinksWithBanPeople();
				else 
					return getCountDeligateOnTableLinksWithoutBanPeople();
			}
			
			public String getNumOfTable(){
				if(infoAboutExtraTable())
					return getNumOfTableWithExtra();
				else 
					return getNumOfTableWithoutExtra();
			}
			
			public String getCountTwoLink(By element1,By element2){
				int numberLink = driver.findElements(element1).size();
				int numberBanned = driver.findElements(element2).size();
				numberLink = numberLink + numberBanned;
				String numberLinks = Integer.toString(numberLink);
				return numberLinks;
			}
			
			public String getCountDeligateOnTableLinksWithBanPeople(){
				return getCountTwoLink(deligateOnTheTableLinks,countOfBannedLink);
			}
			
			public String getSizeOfElem(By locator){
				int numberLink = driver.findElements(locator).size();
				String numberLinks = Integer.toString(numberLink);
				return numberLinks;
			}
			
			public String getCountDeligateOnTableLinksWithoutBanPeople(){
				return getSizeOfElem(deligateOnTheTableLinks);
			}
			
			public String getDeligateWithoutTable(){
				return getRegexText(numberOfDelegateWithoutTableHead);
			}
			
			public String getCountOfDeligateWithoutTable(){
				return getCountTwoLink(deligateWithoutTableFirstPartLinks, deligateWithoutTableSecondPartLink);
			}
			
			public boolean infoAboutExtraTable(){
				return locatorIsExist(countExtraTableHead);
			}
			
			public boolean infoAboutBannedPeople(){
				return locatorIsExist(countOfBannedLink);
			}
			
			public String getNumOfTableWithoutExtra(){
				return getRegexText(hostedTableAllHead);
			}
			
			public String getNumOfTableWithExtra(){
				String text = getRegexText(hostedTableAllHead);
				int resuld = Integer.parseInt(text);
				String textExtra = getRegexText(countExtraTableHead);
				int resuldExtra = Integer.parseInt(textExtra);
				resuld=resuld+resuldExtra;
				String countTable = Integer.toString(resuld);
				return countTable;
			}
			
			public String getCountOfTable(){
				return getSizeOfElem(countOfTablesLinks);
			}
			
			public String getCountOfPartnerHead(){
				return getRegexText(countOfPartnerHead);
			}
			
			public String getCountPartnerLink(){
				return getSizeOfElem(countOfPartnerLink);
			}
			
			public String getCountPeopleInTable(){
				String count = driver.findElement(countOfTableHead).getText();
				return count;
			}
			
			public  int getNumberOfTable(){
				int  number = driver.findElements(countOfTablesLinks).size();
				return number;
			}
			
			public String getCountPeopleInTables(String number){
				String numberPeopleOfTable = "(//th/div/div[@class='col-sm-6 text-right'])"+"[" + number + "]";
				String text = driver.findElement(By.xpath(numberPeopleOfTable)).getText();
				text=text.replaceAll("/+\\d+\\d|/+\\d", "");
				return text;
			}
			
			public String getPeopleInTable(String numOfTable){
				String xpathString = "(//div/div/me-table-seating/table)" +"[" + numOfTable + "]/tbody/tr/td[@class]";
				int count = driver.findElements(By.xpath(xpathString)).size();
				String num = Integer.toString(count);
				return num;
			}
			
			public void clickByLink(){
				clickByElement(moreInfoAboutPeopleLink);
				waitElement(infoWindowHead);
			}
}
