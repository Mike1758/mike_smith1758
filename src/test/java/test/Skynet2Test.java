package test;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import test_selenium_java.pages.Skynet2Page;

public class Skynet2Test extends TestBase {
	
	@Test
	public void testAutorization(){
		Skynet2Page test = new Skynet2Page(webDriver);
		test.Autification();
		
		Assert.assertEquals(test.getDeligateOnTableText(), test.getCountDeligateOnTableLinks());
	
		Assert.assertEquals(test.getDeligateWithoutTable(), test.getCountOfDeligateWithoutTable());
	
		Assert.assertEquals(test.getNumOfTable(), test.getCountOfTable());
		
		Assert.assertEquals(test.getCountPartnerLink(), test.getCountOfPartnerHead());
		
		test.clickByLink();
		test.clickByElement(test.lol);
	
		int maxNumTable = test.getNumberOfTable();
		for(int number = 1; number<=maxNumTable; number++){
			String num_str = Integer.toString(number);
			Assert.assertEquals(test.getPeopleInTable(num_str), test.getCountPeopleInTables(num_str));
		}	
	}		
}
