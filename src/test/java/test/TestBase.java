package test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.ScreenshotException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import test_selenium_java.util.PropertyLoader;
import test_selenium_java.util.Browser;
import test_selenium_java.webdriver.WebDriverFactory;

public class TestBase {

	protected WebDriver webDriver;

	protected Browser browser;

	@BeforeClass
	public void init() {

		browser = new Browser();
		webDriver = WebDriverFactory.getInstance(browser);
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@AfterSuite(alwaysRun = true)
	public void tearDown() {
		webDriver.quit();
	}
}
