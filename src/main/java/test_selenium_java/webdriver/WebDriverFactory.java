package test_selenium_java.webdriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import  test_selenium_java.util.Browser;

public class WebDriverFactory { 
	
    public static WebDriver getInstance(Browser browser){
    	
       	WebDriver driver = new ChromeDriver();
    	return driver;
    	
    }
    
}
